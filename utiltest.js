/**
 * just constructor of the Basic Class of users
 * @param fname first name
 * @param lname last name
 * @param age age
 * @constructor
 */
function BasicUserClass(fname, lname, age) {
    this.fname = fname;
    this.lname = lname;
    this.age = parseInt(age);
}

/**
 * easy method for return information about user
 * @returns {*}
 */
BasicUserClass.prototype.getInformation = function () {
    return "This is " + this.fname + " " + this.lname + " and his age is " + this.age;
};

/**
 * child class of basic user
 * @param fname first name
 * @param lname last name
 * @param age age
 * @param pass password
 * @constructor
 */
function AdminUser(fname, lname, age, pass) {
    BasicUserClass.apply(this, arguments); // execute a basic class constructor
    this.pass = pass;
    this.fname = 'Ovveride name is ' + fname;//можем переопределить свойство
}

AdminUser.prototype = Object.create(BasicUserClass.prototype);//extends of basic class
AdminUser.prototype.constructor = AdminUser; // our constructor

/**
 * override method of basic class
 * это и есть полиморфиз!!!
 * переопределение метода
 * @returns {*}
 */
AdminUser.prototype.getInformation = function () {
    return "This user is an admin and his name is " + this.fname + " " + this.lname + "  and age is " + this.pass + " and pass is " + this.pass
};

var StaticClass = Object.create({});//закрываем конструктор(нельзя инициилизировать объект)

StaticClass.getInformation = function (fname, lname, age, pass) {
    if (pass) {
        this.user = new AdminUser(fname, lname, age, pass);
    } else {
        this.user = new BasicUserClass(fname, lname, age);
    }
    return this.user.getInformation();
};


//var admin = new AdminUser('Vlad', 'Pogorelov', 22, 'adminpass');
//var user = new BasicUserClass("User", 'POgorelovUser', '30');
//console.log(user.getInformation());
//console.log(admin.getInformation());

console.log(StaticClass.getInformation('Vlad', 'Pogorelov', 22, 'adminpass'));
///var sc = new StaticClass();//error!!!
//console.log(sc.getInformation());