var db = require('db')
var log = require('logger')(module);
function User(name) {
    this.name = name;
}

function AllUsers() {
    this.count = 0;
}
User.prototype = new AllUsers();
AllUsers.prototype.getCount = function () {
    return this.count;
};
User.prototype.hello = function (who) {
    log(db.getPhrases('Hello') + ", " + who.name);
    this.count++;

};
module.exports = User;