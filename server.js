function Foo(name) {
    Foo.age = name;
}

Foo.getName = function () {
    if (Foo.age) {
        return Foo.age;
    } else {
        return 'static';
    }
}

var foo = new Foo('asd');
console.log(Foo.getName());